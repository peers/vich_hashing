#include <string>

#define VICH_TESTING

#include "vich_hashing.hpp"

struct TestHasher
{
  int operator()(std::string const& str) const noexcept {
    if (str == "Francis") { return 0; }
    if (str == "Don")     { return 2; }
    if (str == "Leo")     { return 0; }
    if (str == "Mike")    { return 2; }
    if (str == "Jeff")    { return 0; }
    if (str == "Dan")     { return 8; }
    if (str == "Gary")    { return 7; }
    if (str == "Wen")     { return 8; }
    if (str == "Sharon")  { return 6; }
    if (str == "Dieter")  { return 1; }
    return -1;
  }
};

int main()
{
    hashing::Vich<std::string, int, TestHasher> table{9};
    table.insert("Francis", 1);
    table.insert("Don", 2);
    table.insert("Leo", 3);
    table.insert("Mike", 4);
    table.insert("Jeff", 5);
    table.insert("Dan", 6);
    table.insert("Gary", 7);
    table.insert("Wen", 8);
    table.insert("Sharon", 9);

    table.debug_out();
}
