cmake_minimum_required(VERSION 2.8)
project(vich_hashing)

include_directories(include)

set(CMAKE_CXX_FLAGS "-std=c++17 -Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

set(SOURCES
    src/main.cpp)

add_executable(${PROJECT_NAME} ${SOURCES})
