/**
 * This is an implementation of the VICH hashing algorithms which is based on
 * the papers:
 *   - 'Implementations for Coalesced Hashing', by Jeffrey Scott Vitter
 *   - 'Deletion Algorithms for Coalesced Hashing', by W. C. CHEN and
 *        J. S. Vitter
 *
 * v0.1 - 2018-05-19
 *
 * The most current version can be found under:
 *    https://bitbucket.org/peers/vich_hashing
 *
 * Copyright 2018 Peer Schneider
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef PAPER_CHEN_VITTER_H
#define PAPER_CHEN_VITTER_H

#include <functional>
#include <string>
#include <optional>
#include <vector>
#include <cmath>
#include <iostream>


namespace hashing
{
  /**
   * @brief A hash table implementing Variable Insert Coalesced Hashing (VICH).
   *
   * Glossary
   * ========
   *
   *    Record
   *   ---------------
   *    A data item which is stored in the hash table.
   *
   *
   *    Key
   *   ---------------
   *    Special field inside the record which uniquely identifies the record.
   *
   *
   *    m
   *   ---------------
   *    The number of slots inside a hash table which serve as range of the
   *    hash function.
   *
   *
   *    Hash function
   *   ---------------
   *    A function which maps all possible keys onto the number of slots
   *
   *
   *    Hash address
   *   ---------------
   *    The hash function produces a hash address for a key. This hash
   *    address is unique for a specific key but not unique for all keys.
   *
   *
   *    Collision
   *   ---------------
   *    If the hash function produces the same hash address for two
   *    different keys then this is called a collission.
   *
   *
   *    Slot
   *   ---------------
   *    A position inisde a hash table where a record together with its
   *    auxiliarry data is stored (storage location)
   *
   *
   *    Address Range
   *   ---------------
   *    The slots which serve as the range for the hash function.
   *
   *
   *    Cellar
   *   ---------------
   *    A number of slots used to only store collided records.
   *
   *
   *    mp
   *   ---------------
   *    The total number of slots inside the hash table. The sum of slots of
   *    the address range and the cellar
   *
   *      mp - m = number of slots inside the cellar
   *
   *
   *    n
   *   ---------------
   *    The number of slots filled in a hash table
   *
   *
   *    Load Factor
   *   ---------------
   *    The load factor is defined as the ratio of the number of records
   *    stored in the hash table and the number of available slots (including
   *    the cellar) in the hash table.
   *
   *      load_factor = n / mp
   *
   *
   *    Address Factor
   *   ---------------
   *    The relative size of the address region and the cellar.
   *
   *      address_factor = m / mp
   *
   *    An address factor of 0.86 has been shown to yield a good performance
   *    for most load factors.
   *
   *
   *    L
   *   ---------------
   *    Expected number of inserted records needed to make the cellar to
   *    become full.
   *
   *
   *    lambda
   *   ---------------
   *    lambda = L / m
   *
   *       e^-lambda + lambda = 1 / address factor
   *
   *      1) alpha < lambda * address factor
   *
   *         With high probability not enough records have been
   *         inserted to fill up the cellar.
   *
   *      2) alpha > lambda * address factor
   *
   *         Enough records have been inserted to make the cellar
   *         almost surely full.
   *
   */
  template <class Key,
            class T,
            class Hash = std::hash<Key>,
            class KeyEqual = std::equal_to<Key>
           >
  class Vich
  {
    static constexpr double c_default_address_factor{0.86};

    //
    // The number of slots used for administrative purpose at the beginning
    // of the hash table. These slots are not available for real records.
    //
    static constexpr int c_num_admin_slots{2};

    //
    // The index of the table slot which is used as a dummy for the double
    // linked list which stores all colliders of the address region.
    //
    static constexpr int c_dbll_idx{0};

    //
    // The index of the table slot which is used as a sentinel.
    //
    static constexpr int c_end_idx{1};

  public:
    using key_type = Key;
    using mapped_type = T;
    using size_type = std::int64_t;
    using difference_type = std::int64_t;
    using index_type = size_type;
    using hasher = Hash;
    using key_equal = KeyEqual;

    enum class Result { success, failure, duplicate, overflow };

    /**
     * Computes the cellar count based on the address region count and the
     * address factor.
     *
     * @param   slot_count  The number of address region slots.
     * @param   address_factor  The address factor.
     * @return  The cellar count based on slot_count and address_factor.
     */
    static auto cellar_count(size_type slot_count, double address_factor) {
      auto arc = static_cast<double>(slot_count);
      auto c = ((arc * (1 - address_factor)) / address_factor);
      return std::lround(std::ceil(c));
    }

    /** @name Construction
     *
     *  Constructs a VICH hash table
     */
    ///@{
    /**
     * You can not default construct a hash table, you have to specify at
     * least the initial slot count.
     */
    Vich() = delete;

    /**
     * Constructs a VICH hash table with the specified number of address
     * region slots. The number of cellar slots is determined using the
     * default address factor.
     *
     * @note    Ideally we do not change the number of slots after creating a
     *          VICH hash table because rehasing is expensive.
     *
     * @param   slot_count  The initial number of address region slots.
     */
    Vich(size_type slot_count);

    /**
     * Constructs a VICH hash table with the specified number of address
     * region and cellar slots.
     * The address factor is computed based on the specified number of slots.
     *
     * @note    Ideally we do not change the number of slots after creating a
     *          VICH hash table because rehasing is expensive.
     *
     * @param   slot_count  The initial number of address region slots.
     * @param   cellar_count  The initial number of cellar slots.
     */
    Vich(size_type slot_count, size_type cellar_count);
    ///@}

    /** @name Insertion
     *
     * Inserts the specified value identified by the specified key into
     * the hash table. If an entry for the specified key already exists then
     * the call be aborted and the hash table will not be changed.
     *
     * @note If there is not enough space inside the hash table in order
     * to store the specified information then the table does not automatically
     * gets rehashed. Rehashing has to be done manually.
     *
     * @sa insert_or_assign
     */
    ///@{
    ///
    /**
     * @param key   The key identifying the value.
     * @param v     The value which shall be inserted into the hash table.
     * @return  Result::success if inserting was successful, returns
     *          Result::duplicate if there is already an entry for the
     *          specified key and returns overflow ithe the information could
     *          not be inserted because the hash table is full.
     */
    auto insert(key_type const& key, mapped_type const& v) noexcept -> Result;

    /**
     * @param key   The key identifying the value.
     * @param v     The value which shall be inserted into the hash table.
     * @return  Result::success if inserting was successful, returns
     *          Result::duplicate if there is already an entry for the
     *          specified key and returns overflow ithe the information could
     *          not be inserted because the hash table is full.
     */
    auto insert(key_type const& key, mapped_type&& v) noexcept -> Result;

    /**
     * @param key   The key identifying the value.
     * @param v     The value which shall be inserted into the hash table.
     * @return  Result::success if inserting was successful, returns
     *          Result::duplicate if there is already an entry for the
     *          specified key and returns overflow ithe the information could
     *          not be inserted because the hash table is full.
     */
    auto insert(key_type&& key, mapped_type&& v) noexcept -> Result;
    ///@}

    /** @name Insertion or Assign
     *
     * Inserts the specified value identified by the specifed key into the
     * hash table. If an entry for the specified key already exists then
     * the value of the existing entry will be replaced by the new value.
     *
     * @note If there is not enough space inside the hash table in order
     * to store the specified information then the table does not automatically
     * gets rehashed. Rehashing has to be done manually.
     *
     * @sa insert_or_assign
     */
    ///@{
    /**
     * @param key   The key identifying the value.
     * @param v     The value which shall be inserted or assigned.
     * @return  Returns Result::success if inserting the key value pair or,
     *          in case an entry for the specified key existed, assigning the
     *          value succeeded. If insertion was not possible because the
     *          hash table is full already, then the function returns
     *          Result::overflow.
     */
    auto insert_or_assign(key_type const& key, mapped_type&& v) noexcept
      -> Result;

    /**
     * @param key   The key identifying the value.
     * @param v     The value which shall be inserted or assigned.
     * @return  Returns Result::success if inserting the key value pair or,
     *          in case an entry for the specified key existed, assigning the
     *          value succeeded. If insertion was not possible because the
     *          hash table is full already, then the function returns
     *          Result::overflow.
     */
    auto insert_or_assign(key_type&& key, mapped_type&& v) noexcept -> Result;
    ///@}


    /** @name has
     *
     * Returns whether there is an entry for the specified key inside the
     * hash table.
     *
     * @param key   The key for which we want to know whether a hash table
     *              entry exists.
     * @return  true if a hash table entry exists for the key, false otherwise.
     */
    ///@{
    [[nodiscard]] auto has(key_type const& key) const noexcept -> bool;
    ///@}

    /** @name Accessing a hash table entry
     *
     * Returns a reference or a pointer to the value of the entry
     * identified by the specified key.
     */
    ///@{
    /**
     * @param key   The key for which we want to access a value.
     * @return  A constant reference to the value identifed by the key.
     * @exception If there is no entry for the specified key a
     *            std::out_of_range exception will be thrown.
     */
    [[nodiscard]] auto at(key_type const& key) const -> mapped_type const&;

    /**
     * @param key   The key for which we want to access a value.
     * @return  A none constant reference to the value identifed by the key.
     * @exception If there is no entry for the specified key a
     *            std::out_of_range exception will be thrown.
     */
    [[nodiscard]] auto at(key_type const& key) -> mapped_type&;

    /**
     * @param key   The key for which we want to access a value.
     * @return  A constant pointer to the value identifed by the key. If no
     *          entry for the specified key exists the return pointer is
     *          nullptr.
     */
    [[nodiscard]] auto find(key_type const& key) const noexcept
      -> mapped_type const*;

   /**
    * @param key   The key for which we want to access a value.
    * @return  A constant pointer to the value identifed by the key. If no
    *          entry for the specified key exists the return pointer is
    *          nullptr.
    */
    [[nodiscard]] auto find(key_type const& key) noexcept -> mapped_type*;
    ///@}

    /** @name Erasing data items
     *
     * Erases the Record identified by the specified key.
     *
     * @param key   The key identifying the data item which shall be erased.
     * @return  Returns true if a data item actually got erased, false
     *          otherwise.
     */
    ///@{
    auto erase(key_type const& key) -> Result;
    ///@}

    /** @name Rehashing the hash table
     *
     * Rehashing is the process of increasing or decreasing the number of
     * slots and reinserting all data items into the resized hash table.
     *
     * The minimum number of slots we need in a hash table in order to store
     * a specific number of data items depends on the load factor.
     * If we have a number of data items stored and if we have a maximum load
     * factor then there is a minimum number of slots we need in the hash
     * table in order to store the data items.
     * If now the number of slots shall be decreased to a number which is less
     * than the minimum number of slots needed then this is not possible and
     * rehashing fails.
     */
    ///@{
    /**
     * @param slot_count  The new number of address region slots.
     * @return  Returns true if rehasing was possible and successful, false
     *          otherwise.
     */
    auto rehash(size_type slot_count) -> bool;

    /**
     * @param slot_count  The new number of address region slots.
     * @param cellar_count  The new number of cellar slots.
     * @return
     */
    auto rehash(size_type slot_count, size_type cellar_count) -> bool;
    ///@}

    /** @name Load Factor
     *
     * The load factor is the ratio of the number of records stored in
     * the hash table and the number of available slots (including the cellar
     * slots) in the hash table.
     */
    ///@{
    /**
     * @return  Returns the current load factor of the hash table.
     */
    [[nodiscard]] auto load_factor() const noexcept -> double;
    ///@}

    /** @name The Address Factor
     *
     * The address factor is the the relative size of the address region and
     * the cellar.
     */
    ///@{
    /**
     * Sets a new address factor (range ]0.0, 1.0]).
     *
     * A changed address factor may result in a changed number of cellar slots.
     * If this is the case then the hash table gets automatically rehashed.
     *
     * @param factor  The new address factor.
     * @return  Returns true if the hash table had to be rehashed, false
     *          otherwise.
     */
    bool set_address_factor(double factor);

    /**
     * @return  Returns the current address factor.
     */
    [[nodiscard]] auto address_factor() const noexcept -> double;
    ///@}

    /** @name General Information
     *
     */
    ///@{
    /**
     * @return  Returns the number of currently in the hash table stored
     *          records.
     */
    [[nodiscard]] auto size() const noexcept -> size_type;

    /**
     * @return  Returns whether the hash table is empty or not.
     */
    [[nodiscard]] auto empty() const noexcept -> bool;

    /**
     * @return  Returns the number of slots (including the cellar) of the
     *          hash table.
     */
    [[nodiscard]] auto capacity() const noexcept -> size_type;
    ///@}

#ifdef VICH_TESTING
    /**
     * @brief debug_out
     */
    void debug_out() const;

    /**
     *  @brief debug_find
     */
    auto debug_find(key_type const& k) const noexcept
    -> std::pair<index_type, mapped_type>;
#endif // VICH_TESTING

  private:
    /**
     * @brief Vich
     * @param address_region_count
     * @param cellar_count
     * @param address_factor
     */
    Vich(size_type address_region_count, size_type cellar_count,
         double address_factor);

    class Slot
    {
    public:

      struct Record {
        template <typename K, typename V>
        Record(K&& k, V&& v) noexcept
          : key{std::forward<K>(k)}, value{std::forward<V>(v)} {}

        key_type key;
        mapped_type value;
      };

      using record_type = Record;

      template <typename U>
      void set_record(U&& r) noexcept {
        _record.emplace(std::forward<U>(r));
      }

      template <typename ... Args>
      inline void set_record(Args&& ... args) noexcept {
        _record.emplace(std::forward<Args>(args)...);
      }

      template <typename U>
      inline void set_value(U&& r) noexcept {
        _record->value = std::forward<U>(r);
      }

      index_type _link{c_end_idx};

      // These two fields are used for two things, a) a double linked list
      // storing all colliders in the address region (colliders of one specific
      // hash address???) and b) to have a double linked list of all available
      // slots.
      index_type _prev{c_end_idx};
      index_type _next{c_end_idx};

      std::optional<record_type> _record;
    };

    using slot_type = Slot;

    /**
     * @brief init_table
     *
     * Initialise the specified table.

     * @param table
     * @param mp
     */
    void init_table(std::vector<slot_type>& table, size_type mp) noexcept;

    /**
     * @brief cellar_count
     *
     * Computes the number of cellar slots based on the address region count
     * and the specified address factor. If no address factor is given then
     * the member address factor is used.
     *
     * @param address_region_count
     * @param address_factor
     * @return
     */
    [[nodiscard]] auto cellar_count(size_type address_region_count,
                                    std::optional<double> const& address_factor
                                    = std::nullopt) const noexcept -> size_type;

    /**
     * @brief hash
     *
     * Computes the hash address for the specified key.
     *
     * @param k
     * @return
     */
    inline auto hash(key_type const& k) const noexcept -> index_type
    { return (_hash(k) % _m) + c_num_admin_slots; }

    /**
     * @brief insert_helper
     *
     * A helper method for item insertion. Searches for an entry for the
     * specified key. If an entry for the key is found a pointer to the
     * according slot is returned together with the result 'duplicate'.
     * If no entry is found we try to acquire a free slot. If this succeeds
     * then we return the result 'success' together with a pointer to the
     * acquired slot. In the case where we can  not acquire an empty slot
     * because the table is empty we return 'overflow' together with an empty
     * slot pointer.
     *
     * @param key
     * @return
     */
    [[nodiscard]] auto insert_helper(key_type const& key) noexcept
      -> std::pair<Result, slot_type*>;

    /**
     * @brief rehash_helper
     *
     * Creates a new table with the new number of slots in address region
     * and cellar. Then moves the existing items from the old table into the
     * new table. For each existing table entry we execute an insert operation.
     *
     * @note The resulting table will usually not be identical to a 'fresh'
     *       table because the order in which the data items are inserted
     *       will most likely not be the same.
     * @param address_region_count
     * @param cellar_count
     */
    [[nodiscard]] auto rehash_helper(size_type address_region_count,
                                     size_type cellar_count) -> bool;

    /**
     * @brief Removes a slot from the double linked list chaining up all
     *        empty slots.
     *
     * @param i   The index of the slot.
     */
    void remove_avail(index_type i) noexcept;

    /**
     * @brief Inserts a slot the double linked list chaining up all colliders
     *        from the address area.
     *
     * @param i   The index of the slot.
     */
    void insert_collider(index_type i) noexcept;

    /**
     * @brief Returns a slot to the available-slot list.
     *
     * Depending on whether the specified slot is in the cellar or the
     * address region the slot will be returned to the beginning or the end
     * of the available-slot list. If it is a cellar slot then it will be
     * added to the beginning of the list, otherwise to the end.
     *
     * @param i   The index of the slot.
     */
    void return_avail(index_type i) noexcept;

    /**
     * @brief   Removes a slot from the list of address range colliders.
     * @param   i   The index of the slot.
     */
    void remove_collider(index_type i) noexcept;

    // number of records currently stored in the hash table
    size_type _n{};

    // the number of slots in the address region of the hash table
    size_type _m;

    // the number of slots in the hash table (address region plus cellar)
    size_type _mp;

    // The storage where we store all the data items to.
    std::vector<slot_type> _table;

    hasher _hash;

    double _address_factor{c_default_address_factor};
  };

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  Vich<Key, T, Hash, KeyEqual>
  ::Vich(size_type address_region_count, size_type cellar_count,
         double address_factor)
    : _m{address_region_count}
    , _mp{address_region_count + cellar_count}
    , _table{static_cast<size_t>(_mp + c_num_admin_slots)}
    , _address_factor{address_factor}
  {
    init_table(_table, _mp);
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  Vich<Key, T, Hash, KeyEqual>
  ::Vich(size_type slot_count)
    : Vich<Key, T, Hash, KeyEqual>{
        slot_count,
        cellar_count(slot_count, c_default_address_factor),
        c_default_address_factor}
  { }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  Vich<Key, T, Hash, KeyEqual>
  ::Vich(size_type slot_count, size_type cellar_count)
    : Vich<Key, T, Hash, KeyEqual>{
        slot_count, cellar_count,
        static_cast<double>(slot_count) /
        static_cast<double>(slot_count + cellar_count)}
  { }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::insert(key_type const& key, mapped_type const& v) noexcept -> Result {
    auto [result, slot] = insert_helper(key);
    if (result == Result::success) {
      slot->set_record(key, v);
      ++_n;
    }
    return result;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::insert(key_type const& key, mapped_type&& v) noexcept -> Result {
    auto [result, slot] = insert_helper(key);
    if (result == Result::success) {
      slot->set_record(key, std::forward<mapped_type>(v));
      ++_n;
    }
    return result;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::insert(key_type&& key, mapped_type&& v) noexcept -> Result {
    auto [result, slot] = insert_helper(key);
    if (result == Result::success) {
      slot->set_record(std::forward<key_type>(key), std::forward<mapped_type>(v));
      ++_n;
    }
    return result;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::insert_or_assign(key_type const& key, mapped_type&& v) noexcept -> Result
  {
    auto [result, slot] = insert_helper(key);
    if (result == Result::duplicate) {
      slot->set_value(std::forward<mapped_type>(v));
    }
    else if (result == Result::success) {
      slot->set_record(key, std::forward<mapped_type>(v));
      ++_n;
    }
    return result;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::insert_or_assign(key_type&& key, mapped_type&& v) noexcept -> Result
  {
    auto [result, slot] = insert_helper(key);
    if (result == Result::duplicate) {
      slot->set_value(std::forward<mapped_type>(v));
    }
    else if (result == Result::success) {
      slot->set_record(std::move(key), std::forward<mapped_type>(v));
      ++_n;
    }
    return result;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::has(key_type const& key) const noexcept -> bool
  {
    auto s = &_table[hash(key)];
    if (not s->_record.has_value())   { return false; }
    if (s->_record->key == key)       { return true; }
    if (s->_link == c_end_idx)        { return false; }
    while (true) {
      s = &_table[s->_link];
      if (s->_record->key == key)     { return true; }
      if (s->_link == c_end_idx)      { return false; }
    }
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::at(key_type const& key) const -> mapped_type const&
  {
    auto s = &_table[hash(key)];
    if (not s->_record.has_value())   { throw std::out_of_range{"No entry for the specified key."}; }
    if (s->_record->key == key)       { return s->_record->value; }
    if (s->_link == c_end_idx)        { throw std::out_of_range{"No entry for the specified key."}; }
    while (true) {
      s = &_table[s->_link];
      if (s->_record->key == key)     { return s->_record->value; }
      if (s->_link == c_end_idx)      { throw std::out_of_range{"No entry for the specified key."}; }
    }
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::at(key_type const& key) -> mapped_type&
  {
    auto s = &_table[hash(key)];
    if (not s->_record.has_value())   { throw std::out_of_range{"No entry for the specified key."}; }
    if (s->_record->key == key)       { return s->_record->value; }
    if (s->_link == c_end_idx)        { throw std::out_of_range{"No entry for the specified key."}; }
    while (true) {
      s = &_table[s->_link];
      if (s->_record->key == key)     { return s->_record->value; }
      if (s->_link == c_end_idx)      { throw std::out_of_range{"No entry for the specified key."}; }
    }
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::find(key_type const& key) const noexcept -> mapped_type const*
  {
    auto s = &_table[hash(key)];
    if (not s->_record.has_value())   { return nullptr; }
    if (s->_record->key == key)       { return &(s->_record->value); }
    if (s->_link == c_end_idx)        { return nullptr; }
    while (true) {
      s = &_table[s->_link];
      if (s->_record->key == key)     { return &(s->_record->value); }
      if (s->_link == c_end_idx)      { return nullptr; }
    }
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::find(key_type const& key) noexcept -> mapped_type*
  {
    auto s = &_table[hash(key)];
    if (not s->_record.has_value())   { return nullptr; }
    if (s->_record->key == key)       { return &(s->_record->value); }
    if (s->_link == c_end_idx)        { return nullptr; }
    while (true) {
      s = &_table[s->_link];
      if (s->_record->key == key)     { return &(s->_record->value); }
      if (s->_link == c_end_idx)      { return nullptr; }
    }
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::erase(key_type const& key) -> Result
  {
    //////////////////////////////////////////////////////////////////////////
    // Step A1: Search for the record to be deleted.
    auto i = hash(key);
    auto h = i;
    auto pi = c_end_idx;
    if (not _table[i]._record.has_value()) {
      return Result::failure;
    }
    while (_table[i]._record->key not_eq key) {
      if( _table[i]._link == c_end_idx) {
        return Result::failure;
      }
      pi = i;
      i = _table[i]._link;
    }
    // The record to be deleted is in slot i; preceding slot in the
    // chain is in pi.

    //////////////////////////////////////////////////////////////////////////
    // Step A2: slot i is in the address range, followed by a cellar slot
    auto j = _table[i]._link;
    if ((i < (_m + c_num_admin_slots)) && (j >= (_m + c_num_admin_slots))) {
      _table[i].set_record(std::move(*_table[j]._record));
      pi = i;
      i = j;
    }

    //////////////////////////////////////////////////////////////////////////
    // Step A3: slot i is in the cellar
    if (i >= (_m + c_num_admin_slots)) {
      auto first = _table[c_dbll_idx]._next;
      if (first == c_dbll_idx) {
        // The first timewise collider does not exist
        _table[pi]._link = _table[i]._link;
        return_avail(i);
        return Result::success;
      }
      auto hf = hash(_table[first]._record->key);
      _table[i].set_record(std::move(*_table[first]._record));
      if (hf != h) {
        // Link slot i into the chain containing slot first
        _table[pi]._link = _table[i]._link;
        _table[i]._link = _table[hf]._link;
        _table[hf]._link = i;
      }
      pi = i;
      auto k = _table[i]._link;
      i = first;
      while (k not_eq i) {
        pi = k;
        k = _table[k]._link;
      }
    }

    //////////////////////////////////////////////////////////////////////////
    // Step A4: Slot i is in address region; rehash rest of chain
    do {
      auto pc = c_end_idx;
      auto c = c_end_idx;
      auto pj = i;
      j = _table[i]._link;
      while (j != c_end_idx) {
        if (hash(_table[j]._record->key) == i) {
          pc = pj;
          c = j;
        }
        pj = j;
        j = _table[j]._link;
      }
      // Slot c stores the (timewise) first record with hash address i
      if (c == c_end_idx) {
        break;
      }
      _table[i].set_record(std::move(*_table[c]._record));
      _table[pc]._link = c_end_idx;
      remove_collider(c);
      i = c;
    } while (true);
    if (pi != c_end_idx) {
      remove_collider(_table[pi]._link);
      _table[pi]._link = _table[i]._link;
    }
    return_avail(i);
    return Result::success;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::rehash(size_type address_region_count) -> bool
  {
    return rehash_helper(address_region_count,
                         cellar_count(address_region_count));
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::rehash(size_type address_region_count, size_type cellar_count) -> bool
  {
    _address_factor = static_cast<double>(address_region_count) /
                      static_cast<double>(address_region_count + cellar_count);
    return rehash_helper(address_region_count, cellar_count);
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::load_factor() const noexcept -> double
  {
    return static_cast<double>(_n)/static_cast<double>(_mp);
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::set_address_factor(double factor) -> bool
  {
    _address_factor = factor;
    auto new_cellar_size = cellar_count(_m, factor);
    if (new_cellar_size != (_mp - _m)) {
      rehash(_m);
      return true;
    }
    return false;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::address_factor() const noexcept -> double
  {
    return _address_factor;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::size() const noexcept -> size_type
  {
    return _n;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::empty() const noexcept -> bool
  {
    return (_n == 0);
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::capacity() const noexcept -> size_type
  {
    return _mp;
  }

  //----------------------------------------------------------------------------

#ifdef VICH_TESTING
  template <class Key, class T, class Hash, class KeyEqual>
  void Vich<Key, T, Hash, KeyEqual>
  ::debug_out() const
  {
    for (int i = 0; i < _mp; ++i) {
      std::cout << (i + 1) << ": ";
      if (_table[i+2]._record.has_value()) {
         std::cout << _table[i+2]._record->key;
      }
      std::cout << '\n';
    }
    std::cout << std::endl;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::debug_find(key_type const& k) const noexcept
  -> std::pair<index_type, mapped_type>
  {
    for (index_type i = 0; i < static_cast<index_type>(_table.size()); ++i) {
      auto const& slot = _table[i];
      if (slot._record.has_value() && (slot._record->key == k)) {
        return std::make_pair(i, slot._record->value);
      }
    }
    return std::make_pair(-1, mapped_type{});
  }
#endif // VICH_TESTING

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  void Vich<Key, T, Hash, KeyEqual>
  ::init_table(std::vector<slot_type>& table, size_type mp) noexcept
  {
    // Initialise the double linked list which will store all colliders stored
    // in the address region.
    table[c_dbll_idx]._next = c_dbll_idx;
    table[c_dbll_idx]._prev = c_dbll_idx;

    // Initialises the double linked list which stores all available empty
    // slots.
    table[c_end_idx]._next = mp - 1 + c_num_admin_slots;
    table[mp - 1 + c_num_admin_slots]._prev = c_end_idx;
    for (index_type i = 0; i < mp; ++i) {
      auto ip = i + c_num_admin_slots;
      table[ip]._next = ip -1;
      table[ip - 1]._prev = ip;
    }
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::cellar_count(size_type address_region_count,
                 std::optional<double> const& address_factor) const noexcept
  -> size_type
  {
    // beta = M / M'
    // c = M' - M  =>  M' = c + M
    // => beta = M / (c + M)
    // => beta * c + beta * M = M
    // => beta * c = M - beta * M = M * (1 - beta);
    // => c = (M * (1 - beta)) / beta

    auto b = (bool(address_factor) ? *address_factor : _address_factor);
    auto c = (static_cast<double>(address_region_count) * (1 - b)) / b;
    c = std::lround(std::ceil(c));

    return c;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::insert_helper(key_type const& key) noexcept -> std::pair<Result, slot_type*>
  {
    auto i = hash(key);
    if (not _table[i]._record.has_value()) {
      // Search is unsuccesful, the slot is empty. We return the i'th slot
      // so the inserting member can fill in the data.
      remove_avail(i);
      // The first element in the chain got inserted, this means there is yet
      // no chain.
      _table[i]._link = c_end_idx;
    }
    else {
      // There is a collision, the chain must be searched
      auto j = i; // j will hold the index of
      do {
        if (_table[i]._record->key == key) {
          return std::make_pair(Result::duplicate, &_table[i]);
        }
        if (_table[i]._link == c_end_idx) {
          break;
        }
        i = _table[i]._link;
        if (i >= (_m + c_num_admin_slots)) {
          j = i;
        }
      } while (true);
      // The search was unsuccessful, so the record will be inserted.
      // First, however, an empty slot must be found to store the record.
      auto r = _table[c_end_idx]._next;
      if (r == c_end_idx) {
        return std::make_pair(Result::overflow, nullptr);
      }
      remove_avail(r);
      if (r < (_m + c_num_admin_slots)) {
        insert_collider(r);
      }
      // Link the r'th slot into the chain after the j'th slot
      _table[r]._link = _table[j]._link;
      _table[j]._link = r;
      i = r;
    }
    return std::make_pair(Result::success, &_table[i]);
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  auto Vich<Key, T, Hash, KeyEqual>
  ::rehash_helper(size_type address_region_count, size_type cellar_count)
  -> bool
  {
    if ((address_region_count + cellar_count) < _n) {
      return false;
    }

    std::vector<slot_type> table{static_cast<size_t>(address_region_count +
                                                cellar_count +
                                                c_num_admin_slots)};
    init_table(table, (address_region_count + cellar_count));

    std::swap(table, _table);

    _n = 0;
    _m = address_region_count;
    _mp = address_region_count + cellar_count;

    // Iterate over the old table and insert every existing data item into
    // the new table.
    for (auto& s : table) {
      if (s._record.has_value()) {
        insert(std::move(s._record->key), std::move(s._record->value));
      }
    }

    return true;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  void Vich<Key, T, Hash, KeyEqual>
  ::remove_avail(index_type i) noexcept
  {
    _table[_table[i]._next]._prev = _table[i]._prev;
    _table[_table[i]._prev]._next = _table[i]._next;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  void Vich<Key, T, Hash, KeyEqual>
  ::insert_collider(index_type i) noexcept
  {
    _table[i]._prev = _table[c_dbll_idx]._prev;
    _table[c_dbll_idx]._prev = i;
    _table[i]._next = c_dbll_idx;
    _table[_table[i]._prev]._next = i;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  void Vich<Key, T, Hash, KeyEqual>
  ::return_avail(index_type i) noexcept
  {
    auto spot = ((i >= (_m + c_num_admin_slots)) ? c_end_idx
                                                 : _table[c_end_idx]._prev);
    _table[i]._record.reset();
    _table[i]._record = std::nullopt;
    _table[i]._next = _table[spot]._next;
    _table[spot]._next = i;
    _table[i]._prev = spot;
    _table[_table[i]._next]._prev = i;
    --_n;
  }

  //----------------------------------------------------------------------------

  template <class Key, class T, class Hash, class KeyEqual>
  void Vich<Key, T, Hash, KeyEqual>
  ::remove_collider(index_type i) noexcept
  {
    remove_avail(i);
  }
}

#endif // PAPER_CHEN_VITTER_H
